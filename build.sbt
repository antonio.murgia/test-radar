name := "xls2radar"
organization := "it.agilelab"

scalaVersion := "2.13.3"

val circeVersion = "0.12.3"

libraryDependencies ++= Seq(
  "org.apache.poi" % "poi"               % "4.1.2",
  "org.apache.poi" % "poi-ooxml"         % "4.1.2",
  "org.apache.poi" % "poi-ooxml-schemas" % "4.1.2"
)

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies ++= Seq(
  "org.apache.velocity" % "velocity"       % "1.7",
  "org.apache.velocity" % "velocity-tools" % "2.0"
)

libraryDependencies += "com.monovore" %% "decline" % "1.3.0"

enablePlugins(JavaAppPackaging)
