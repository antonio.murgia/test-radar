package it.agilelab.techradar

import java.io.PrintWriter
import java.nio.file.{ Path, Paths }
import java.time.ZonedDateTime

import cats.implicits.catsSyntaxTuple3Semigroupal
import com.monovore.decline.{ CommandApp, Opts }
import io.circe.generic.auto._
import io.circe.syntax._
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.runtime.RuntimeConstants
import org.apache.velocity.runtime.resource.loader.FileResourceLoader

import scala.jdk.CollectionConverters.IterableHasAsScala
object MainFunctions {
  def readXlsFile(file: Path): (Map[String, Int], Map[Level, Int], List[RadarEntry]) = {
    val wb = WorkbookFactory.create(file.toFile)
    try {
      val sheet     = wb.getSheetAt(0).asScala.toList
      val quadrants =
        sheet.drop(1).map(_.getCell(1).getStringCellValue.toLowerCase().capitalize).distinct.zipWithIndex.toMap
      val levels    = List(
        Level("ADOPT".toLowerCase().capitalize, "#93c47d"),
        Level("TRIAL".toLowerCase().capitalize, "#93d2c2"),
        Level("ASSESS".toLowerCase().capitalize, "#fbdb84"),
        Level("HOLD".toLowerCase().capitalize, "#efafa9")
      ).zipWithIndex.toMap

      val levelMap = levels.map { case (k, v) =>
        k.name -> v
      }

      val entries = sheet.drop(1).map { r =>
        RadarEntry(
          quadrant = quadrants(r.getCell(1).getStringCellValue.toLowerCase().capitalize),
          ring = levelMap(r.getCell(2).getStringCellValue.toLowerCase().trim.capitalize),
          label = r.getCell(0).getStringCellValue,
          active = true,
          moved = 0,
          link = Option(r.getCell(4)).map(_.getStringCellValue).getOrElse("")
        )
      }
      (quadrants, levels, entries)
    } finally wb.close()
  }
  def makeContext(
    quadrants: Map[String, Int],
    levels: Map[Level, Int],
    entries: List[RadarEntry],
    now: ZonedDateTime
  ): VelocityContext = {
    val context = new VelocityContext()
    context.put("title", s"AgileLab Tech Radar — ${now.getYear}.${now.getMonthValue}")
    context.put("quadrants", quadrants.keys.map(Named).toList.asJson.noSpaces)
    context.put("rings", levels.keys.asJson.noSpaces)
    context.put("entries", entries.asJson.noSpaces)
    context
  }

  def main: Opts[Unit] = {
    val sourceOpt   = Opts.option[String]("source", help = "Source xls file.")
    val targetOpt   = Opts.option[String]("target", help = "Target html file.")
    val templateOpt = Opts
      .option[String]("template", help = "Velocity template file.")
      .withDefault("template.html")

    (sourceOpt, targetOpt, templateOpt).mapN { (src, target, template) =>
      val (quadrants, levels, entries) = MainFunctions.readXlsFile(Paths.get(src))
      val now                          = ZonedDateTime.now()
      val templatePath                 = Paths.get(template)
      val ve                           = MainFunctions.initVelocityEngine(templatePath)
      val t                            = ve.getTemplate(templatePath.getFileName.toString)
      val context                      = MainFunctions.makeContext(quadrants, levels, entries, now)
      val pw                           = new PrintWriter(Paths.get(target).toFile)
      try t.merge(context, pw)
      finally pw.close()
    }
  }
  def initVelocityEngine(templatePath: Path): VelocityEngine = {
    val ve = new VelocityEngine()
    ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "file")
    ve.setProperty("file.resource.loader.class", classOf[FileResourceLoader].getName)
    ve.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, templatePath.getParent.toAbsolutePath.toString)
    ve.init()
    ve
  }
}
object Main
    extends CommandApp(
      name = "xlsToTechRadar",
      header = "Transform our CTO beloved excel files into a tech radar site",
      main = MainFunctions.main
    )
case class Named(name: String)
case class Level(name: String, color: String)
case class RadarEntry(quadrant: Int, ring: Int, label: String, active: Boolean, link: String, moved: Int)
