#!/usr/bin/env sh
set -eax
export SW_VERSION=$(cat ./target/universal/stage/version.txt | tr -d ' ')
docker build -t $DOCKER_REPO/xls2radar:$SW_VERSION .
docker build -t $DOCKER_REPO/xls2radar:latest .
docker push $DOCKER_REPO/xls2radar:latest
docker push $DOCKER_REPO/xls2radar:$SW_VERSION