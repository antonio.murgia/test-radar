#!/usr/bin/env sh
set -eax
sbt clean stage
sbt 'inspect actual version' | grep 'Setting: java.lang.String' | cut -d '=' -f2 | tr -d ' ' > ./target/universal/stage/version.txt