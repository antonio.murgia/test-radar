FROM oracle/graalvm-ce:20.2.0-java11
RUN mkdir /app
WORKDIR /app
ADD target/universal/stage/bin bin
ADD target/universal/stage/lib lib